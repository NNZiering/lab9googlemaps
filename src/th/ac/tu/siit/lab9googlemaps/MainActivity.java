package th.ac.tu.siit.lab9googlemaps;

import java.util.Random;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	GoogleMap map;
	LocationManager locManager;
	Location currentLocation;
	double klerap=0;
	PolylineOptions co = new PolylineOptions();
	boolean bleh;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		
		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		map.setMyLocationEnabled(true);
		/*
		//SET A MARKER
		MarkerOptions mo  = new MarkerOptions();
		mo.position(new LatLng(15,100));
		mo.title("Hello");
		
		map.addMarker(mo);
		*/
		//DRAW A LINE
		PolylineOptions po = new PolylineOptions();
		po.add(new LatLng(0,0));
		po.add(new LatLng(13.75,100.4667));
		po.width(5);
		map.addPolyline(po);
		
		//map.setMyLocationEnabled(true);
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location l) {
				currentLocation = l;
				currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				co.add(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				//map.clear();
				map.addPolyline(co);
		

				
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
		
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch(id) {
		case R.id.action_mark:	
			try {
				Toast t = Toast.makeText(this, "MARKED YO", Toast.LENGTH_SHORT);
				t.show();
				MarkerOptions mo  = new MarkerOptions();
				currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				co.add(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				map.addPolyline(co);
				mo.position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				map.addMarker(mo);
			
			} catch (Exception e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			return true;
		case R.id.action_color:	
			try {
				if(bleh)
					co.color(Color.BLUE);
				else
					co.color(Color.RED);
				bleh=!bleh;
				
				map.clear();
				
				MarkerOptions mo  = new MarkerOptions();
				currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				mo.position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
				map.addMarker(mo);
				map.addPolyline(co);
			} catch (Exception e) {
				Toast t = Toast.makeText(this, "Something went wrong", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
